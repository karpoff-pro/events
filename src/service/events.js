import makeGuid from 'uuid/v4';

const EventService = {
  get: () => {
    let items = localStorage.events;

    try {
      items = JSON.parse(localStorage.events);
    } catch (e) {
      items = [];
    }

    if (!Array.isArray(items)) {
      items = [];
    }
    return items;
  },

  add: (data) => {
    const item = {
      id: makeGuid(),
      name: data.name,
      date: data.date,
      city: data.city,
    };

    const items = EventService.get();
    items.push(item);

    localStorage.events = JSON.stringify(items);
  },

  remove: (ids) => {
    const removed = new Set(ids);
    const items = EventService.get().filter(item => !removed.has(item.id));
    localStorage.events = JSON.stringify(items);
  }
};

export default EventService;
