import { createReducer } from 'redux-act';

import { removeEventShowAction, removeEventCancelAction } from '../actions/remove';

const initialState = {
  opened: false,
};

const showReducer = (state) => ({
  ...state,
  opened: true,
});

const hideReducer = () => ({
  ...initialState,
});

export default createReducer({
  [removeEventShowAction]: showReducer,
  [removeEventCancelAction]: hideReducer,
}, initialState);
