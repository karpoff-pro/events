import events from './events';
import add from './add';
import remove from './remove';
import cities from './cities';

export default {
  events,
  add,
  remove,
  cities,
};
