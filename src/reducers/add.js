import { createReducer } from 'redux-act';

import { addEventShowAction, addEventCancelAction, addEventChangeAction } from '../actions/add';

const initialState = {
  opened: false,
  data: {
    city: '',
    cityId: '',
    name: '',
    date: new Date().toISOString().split('T')[0],
  }
};

const showReducer = (state) => ({
  ...state,
  opened: true,
});

const hideReducer = () => ({
  ...initialState,
});

const changeReducer = (state, data) => ({
  ...state,
  data: {
    ...state.data,
    ...data,
  }
});

export default createReducer({
  [addEventShowAction]: showReducer,
  [addEventCancelAction]: hideReducer,
  [addEventChangeAction]: changeReducer,
}, initialState);
