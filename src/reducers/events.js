import { createReducer } from 'redux-act';

import {
  eventListSuccessAction,
  eventFilterAction,
  eventSelectAction,
  eventSelectAllAction,
} from '../actions/events-list';

const initialState = {
  items: [],
  filter: '',
  visibleItems: [],
  selected: new Set(),
};

const getVisibleItems = (items, filter) => {
  const filt = filter.trim().toLowerCase();
  return filt === '' ? items : items.filter(item => item.name.toLowerCase().indexOf(filt) !== -1);
};

const eventsListReducer = (state, payload) => {
  const { items } = payload;
  const visibleItems = getVisibleItems(items, state.filter);

  return eventsFilterReducer({
    ...state,
    items,
    visibleItems,
  }, state.filter);
};

const eventsFilterReducer = (state, filter) => {
  const visibleItems = getVisibleItems(state.items, filter);
  const itemIds = new Set(visibleItems.map(item => item.id));

  return {
    ...state,
    filter,
    visibleItems,
    selected: new Set([...state.selected].filter(s => itemIds.has(s))),
  };
};

const eventSelectReducer = (state, eventId) => {
  const selected = new Set(state.selected);
  if (selected.has(eventId)) {
    selected.delete(eventId);
  } else {
    selected.add(eventId);
  }

  return {
    ...state,
    selected,
  };
};

const eventSelectAllReducer = (state, isSelect) => ({
  ...state,
  selected: new Set(isSelect ? state.visibleItems.map(i => i.id) : []),
});

export default createReducer({
  [eventListSuccessAction]: eventsListReducer,
  [eventFilterAction]: eventsFilterReducer,
  [eventSelectAction]: eventSelectReducer,
  [eventSelectAllAction]: eventSelectAllReducer,
}, initialState);
