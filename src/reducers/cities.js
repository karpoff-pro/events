import { createReducer } from 'redux-act';

const initialState = {
  items: [
    {id: 1, title: 'Нижний Новгород'},
    {id: 2, title: 'Саров'},
    {id: 3, title: 'Арзамас'},
    {id: 4, title: 'Павлово'},
  ],
};

export default createReducer({}, initialState);
