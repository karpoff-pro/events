import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  AppBar,
  Toolbar,
  Typography,
  Paper,
  IconButton,
  Input,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Checkbox
} from 'material-ui';
import { Add as AddIcon, Remove as RemoveIcon } from 'material-ui-icons';

import eventsListAction, {
  eventFilterAction,
  eventSelectAction,
  eventSelectAllAction
} from '../actions/events-list';
import { addEventShowAction } from '../actions/add';
import { removeEventShowAction } from '../actions/remove';

class ListPage extends React.Component {

  componentWillMount() {
    this.props.eventsListAction();
  }

  handleFilterChange = (ev) => {
    this.props.eventFilterAction(ev.target.value);
  };

  handleItemAdd = () => {
    this.props.addEventShowAction();
  };

  handleItemDelete = () => {
    this.props.removeEventShowAction();
  };

  handleEventSelect = (eventId) => {
    this.props.eventSelectAction(eventId);
  };

  handleEventSelectAll = () => {
    this.props.eventSelectAllAction(this.props.items.length !== this.props.selectedItems.size);
  };

  render() {

    const { filter, items, selectedItems } = this.props;

    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <Typography type="title" color="inherit" style={ { margin: 'auto' } }>
              Мероприятия
            </Typography>
          </Toolbar>
        </AppBar>

        <div style={ { padding: '20px' } }>
          <Paper elevation={ 4 }>
            <Toolbar style={ { backgroundColor: "#ddd" } } color={ "#ddd" }>
              <IconButton aria-label="Добавить" onClick={ this.handleItemAdd }>
                <AddIcon />
              </IconButton>
              <IconButton
                aria-label="Удалить"
                disabled={ selectedItems.size === 0 }
                onClick={ this.handleItemDelete }
              >
                <RemoveIcon />
              </IconButton>
              <Typography style={ { flex: 1 } } />
              <Input
                placeholder="Поиск"
                value={ filter }
                onChange={ this.handleFilterChange }
              />
            </Toolbar>

            <Table>
              <TableHead>
                <TableRow>
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={ items.length === selectedItems.size && items.length > 0 }
                      onClick={ this.handleEventSelectAll }
                      disabled={ items.length === 0 }
                    />
                  </TableCell>
                  <TableCell>Название</TableCell>
                  <TableCell>Дата</TableCell>
                  <TableCell>Город</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                { items.map(event => (
                  <TableRow
                    hover
                    role="checkbox"
                    aria-checked={ selectedItems.has(event.id) }
                    tabIndex={ -1 }
                    key={ event.id }
                    selected={ selectedItems.has(event.id) }
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={ selectedItems.has(event.id) }
                        onClick={ () => this.handleEventSelect(event.id) }
                      />
                    </TableCell>
                    <TableCell>{ event.name }</TableCell>
                    <TableCell>{ event.date }</TableCell>
                    <TableCell>{ event.city }</TableCell>
                  </TableRow>
                )) }
              </TableBody>

            </Table>
          </Paper>
        </div>
      </div>
    );
  }
}

ListPage.propTypes = {
  eventsListAction: PropTypes.func.isRequired,
  eventFilterAction: PropTypes.func.isRequired,
  addEventShowAction: PropTypes.func.isRequired,
  removeEventShowAction: PropTypes.func.isRequired,
  eventSelectAction: PropTypes.func.isRequired,
  eventSelectAllAction: PropTypes.func.isRequired,

  items: PropTypes.array.isRequired,
  selectedItems: PropTypes.object.isRequired,
  filter: PropTypes.string,
};

const mapStateToProps = (state) => ({
  items: state.events.visibleItems,
  filter: state.events.filter,
  selectedItems: state.events.selected,
});

const mapDispatchToProps = {
  addEventShowAction,
  removeEventShowAction,
  eventsListAction,
  eventFilterAction,
  eventSelectAction,
  eventSelectAllAction,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListPage);
