import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'material-ui';

import Dialog, {
  DialogActions,
  DialogTitle,
} from 'material-ui/Dialog';

import removeEventApplyAction, { removeEventCancelAction } from '../actions/remove';

class DeletePage extends React.Component {

  handleClose = () => {
    this.props.removeEventCancelAction();
  };

  handleApply = () => {
    this.props.removeEventApplyAction([...this.props.selected]);
  };

  render() {
    return (
      <Dialog onRequestClose={ this.handleClose } open={ this.props.opened }>
        <DialogTitle>Удалить выбранные мероприятия?</DialogTitle>
        <DialogActions>
          <Button onClick={ this.handleClose }>Отмена</Button>
          <Button onClick={ this.handleApply } color="primary">Да</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

DeletePage.propTypes = {
  removeEventCancelAction: PropTypes.func.isRequired,
  removeEventApplyAction: PropTypes.func.isRequired,

  opened: PropTypes.bool.isRequired,
  selected: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  opened: state.remove.opened,
  selected: state.events.selected,
});

const mapDispatchToProps = {
  removeEventCancelAction: removeEventCancelAction,
  removeEventApplyAction: removeEventApplyAction,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeletePage);
