import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TextField, Button, FormControl, Input, InputLabel, Select, MenuItem } from 'material-ui';

import Dialog, {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'material-ui/Dialog';

import addEventApplyAction, { addEventCancelAction, addEventChangeAction } from '../actions/add';

class AddPage extends React.Component {

  constructor(props) {
    super(props);

    this.onNameChange = this.handleChange('name');
    this.onDateChange = this.handleChange('date');
  }

  handleClose = () => {
    this.props.addEventCancelAction();
  };

  handleApply = () => {
    this.props.addEventApplyAction(this.props.data);
  };

  onCityChange = event => {
    this.props.addEventChangeAction({
      ...this.props.data,
      cityId: event.target.value,
      city: this.props.cities.find(city => city.id === event.target.value).title,
    });
  };

  handleChange = name => event => {
    this.props.addEventChangeAction({
      ...this.props.data,
      [name]: event.target.value,
    });
  };

  render() {

    const { opened, cities, data = {} } = this.props;

    const canApply = data.name && data.name.trim() !== '' && data.date && data.city && data.city !== '';
    return (
      <Dialog onRequestClose={ this.handleClose } open={ opened }>
        <DialogTitle>Добавление мероприятия</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Название"
            value={ data.name }
            onChange={ this.onNameChange }
            fullWidth
          />
          <TextField
            margin="dense"
            id="date"
            type="date"
            label="Дата"
            value={ data.date }
            onChange={ this.onDateChange }
            fullWidth
          />

          <FormControl fullWidth>
            <InputLabel htmlFor="event-date">Город</InputLabel>
            <Select
              value={ data.cityId }
              onChange={ this.onCityChange }
              input={ <Input id="event-date" /> }
              fullWidth
            >
              <MenuItem value=""><em>None</em></MenuItem>
              { cities.map(city => (
                <MenuItem key={ city.id } value={ city.id }>{ city.title }</MenuItem>
              )) }
            </Select>
          </FormControl>

        </DialogContent>
        <DialogActions>
          <Button onClick={ this.handleClose }>Отмена</Button>
          <Button
            onClick={ this.handleApply }
            color="primary"
            disabled={ !canApply }
          >
            Добавить
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

AddPage.propTypes = {
  addEventCancelAction: PropTypes.func.isRequired,
  addEventChangeAction: PropTypes.func.isRequired,
  addEventApplyAction: PropTypes.func.isRequired,

  opened: PropTypes.bool.isRequired,
  cities: PropTypes.array.isRequired,
  data: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  opened: state.add.opened,
  data: state.add.data,
  cities: state.cities.items,
});

const mapDispatchToProps = {
  addEventCancelAction,
  addEventChangeAction,
  addEventApplyAction,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddPage);
