/* eslint-disable import/no-named-as-default */
import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

import ListPage from '../containers/list-page';
import AddPage from '../containers/add-page';
import RemovePage from '../containers/remove-page';

const theme = createMuiTheme();

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={ theme }>
        <div>
          <ListPage />
          <AddPage />
          <RemovePage />
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default App;
