
import { createAction } from 'redux-act';
import EventService from '../service/events';

export const eventListStartAction = createAction('event list start');
export const eventListSuccessAction = createAction('event list success');

export default () => (dispatch) => {
  dispatch(eventListStartAction());

  const items = EventService.get();
  dispatch(eventListSuccessAction({ items }));
};

export const eventFilterAction = createAction('event filter');
export const eventSelectAction = createAction('event select');
export const eventSelectAllAction = createAction('event select all');
