import { createAction } from 'redux-act';

import EventService from '../service/events';
import eventsListAction from './events-list';

export const removeEventShowAction = createAction('delete event show');
export const removeEventCancelAction = createAction('delete event cancel');

export default (ids) => (dispatch) => {
  EventService.remove(ids);

  dispatch(removeEventCancelAction());
  dispatch(eventsListAction());
};
