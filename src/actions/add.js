import { createAction } from 'redux-act';

import EventService from '../service/events';
import eventsListAction from './events-list';

export const addEventShowAction = createAction('add event show');
export const addEventCancelAction = createAction('add event cancel');
export const addEventChangeAction = createAction('add event change');

export default (data) => (dispatch) => {
  EventService.add(data);

  dispatch(addEventCancelAction());
  dispatch(eventsListAction());
};
