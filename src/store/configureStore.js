import {createStore, compose, applyMiddleware, combineReducers} from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import createHistory from 'history/createBrowserHistory';
import reducers from '../reducers';

export const history = createHistory();

const logger = createLogger({
  collapsed: true,
});

function configureStoreProd(initialState) {
  return createStore(combineReducers(reducers), initialState, compose(
    applyMiddleware(thunk, logger)
    )
  );
}

function configureStoreDev(initialState) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
  const store = createStore(combineReducers(reducers), initialState, composeEnhancers(
    applyMiddleware(reduxImmutableStateInvariant(), thunk, logger)
    )
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default; // eslint-disable-line global-require
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

const configureStore = process.env.NODE_ENV === 'production' ? configureStoreProd : configureStoreDev;

export default configureStore;
